/*  ----- CONFIGURATION EXAMPLE -----
 *    "job-configuration": {
 *      "timeout": 30000,
 *      "retryOnErrorTimes": 3,
 *      "interval": 120000,
 *      "credentials": "jac",
 *      "searchSize": 50,
 *      "displaySize": 10,
 *      "spaceKeys": ["KEY1","KEY2"],
 *      "playlist": [{"KEY1":"music1"},{"KEY2":"music2"}]
 *    }
 *
 *  --- Auth configuration ---
 *    "jac": {
 *      "server": "https://jira.atlassian.com",
 *      "username": "username",
 *      "password": "password",
 *      "mode": "-header" OR "-user" OR "-lasso"
 *      "lassoServer": "https://id.atlassian.com" //if "lasso" mode
 *    }
 */

var _ = require('underscore'), async = require('async'), cache = require('memory-cache');

var avatars_cache_expiration = 24 * 60 * 60 * 1000;
var pages_cache_expiration = 24 * 60 * 60 * 1000;
var credentials_cache_expiration = 60 * 60 * 1000;

module.exports = function(config, dependencies, job_callback) {

  //Check if at least 1 space key & "credential" are defined
  if (!config.spaceKeys || config.spaceKeys.length == 0 || !config.credentials) {
    return job_callback('Missing parameters in confluenceactivity job');
  }

  //Check if every credential detail is defined
  if (!config.globalAuth || !config.globalAuth[config.credentials] || !config.globalAuth[config.credentials].username || !config.globalAuth[config.credentials].password || !config.globalAuth[config.credentials].server) {
    return job_callback('No credentials found in confluenceactivity job. Please check config.globalAuth');
  }

  //In the specifc case of "Lasso" auth mode, check if the "Lasso" server is defined
  if (config.globalAuth[config.credentials].mode == "-lasso" && !config.globalAuth[config.credentials].lassoServer) {
    return job_callback('No Lasso server found in confluenceactivity job. Please check config.globalAuth');
  }

  //Get the logger instance
  var logger = dependencies.logger;

  //Get the display size (10 by default)
  var displaySize = config.displaySize || 10;

  //Get the search size on each space (50 by default)
  var searchSize = config.searchSize || 50;

  //Seup the request made to Confluence to get the list of recent updates
  var pagesRequest = {
    timeout : config.timeout || 15000
  };

  //Depending on the selected auth mode, add the requried credentials into the data request @pagesRequest
  switch(config.globalAuth[config.credentials].mode) {
    case "-lasso":
      var cookieString = "";
      var cache_key = 'confluenceactivity-lasso-' + config.globalAuth[config.credentials].lassoServer + '-' + config.globalAuth[config.credentials].username + ':cookie';

      //If cookie in cache
      if (!cache.get(cache_key)) {
        //Create the cookie request to send to Lasso server
        var credentialRequest = {
          timeout : config.timeout || 15000,
          url : config.globalAuth[config.credentials].lassoServer + '/id/rest/login',
          method : "POST",
          json : {
            username : config.globalAuth[config.credentials].username,
            password : config.globalAuth[config.credentials].password
          }
        };

        //Send the request & analyse the response
        dependencies.request.post(credentialRequest, function(error, response, credentialsJSON) {
          //If any error
          if (error || !response || (response.statusCode != 200)) {
            var err_msg = (error || ( response ? ("bad statusCode: " + response.statusCode) : "bad response")) + " from " + credentialRequest.url;
            job_callback(err_msg);
          } else {
            //If cookie is not missing
            if (response.headers["set-cookie"] !== undefined) {
              //Get the cookie
              cookieString = response.headers["set-cookie"];
              //Add the cookie into cache
              cache.put(cache_key, cookieString, credentials_cache_expiration);
            } else {
              job_callback("Cannot get the cookie from Lasso server in sprintstatus job.");
            }
          }
          //Put the cookie into the data request
          pagesRequest.cookie = cookieString;
          //Start the job process
          job_process();
        });
      } else {
        //Put the cookie into the data request
        pagesRequest.cookie = cache.get(cache_key);
        //Start the job process
        job_process();
      }
      break;
    case "-user":
      //Put the user credentials into the data request
      pagesRequest.user = {};
      pagesRequest.user[config.globalAuth[config.credentials].username] = config.globalAuth[config.credentials].password;
      //Start the job process
      job_process();
      break;
    case "-header":
    default:
      //Put the auth header into the data request
      pagesRequest.headers = {};
      pagesRequest.headers["Authorization"] = "Basic " + new Buffer(config.globalAuth[config.credentials].username + ":" + config.globalAuth[config.credentials].password).toString("base64");
      //Start the job process
      job_process();
      break;
  }

  // -----------------------
  // Retrieve data from Confluence for 1 space
  // -----------------------
  function retreive_data(spaceKey, callback) {
    //Add to the request the specific URL for this space
    pagesRequest.url = config.globalAuth[config.credentials].server + '/wiki/rest/prototype/latest/search/site.json?' + 'type=page,blog' + '&spaceKey=' + spaceKey + '&pageSize=' + searchSize;

    //Send the request & analyse the response
    dependencies.request(pagesRequest, function(error, response, pagesJSON) {
      //If any error
      if (error || !response || (response.statusCode != 200)) {
        var err_msg = (error || ( response ? ("bad statusCode: " + response.statusCode) : "bad response")) + " from " + pagesRequest.url;
        return callback(err_msg, []);
      } else {
        //Populate "sprintsData" with response JSON data
        var pagesData;
        try {
          pagesData = JSON.parse(pagesJSON);
        } catch (err) {
          return callback('Error parsing JSON response from server', []);
        }

        //Check if at least 1 page is returned
        if (!pagesData.result) {
          return callback(null, []);
        } else {
          //Return the list of pages
          return callback(null, pagesData.result);
        }
      }
    });
  };

  // -----------------------
  // Retrieve base64 data of avatar from Confluence (or cache)
  // -----------------------
  function retreive_avatar(page, callback) {
    var cache_key = 'confluenceactivity-avatar-' + page.lastModifier.name + ':base64';
    var result = [];
    page.lastModifier["avatar64"] = "";

    //Check if already in cache
    if (cache.get(cache_key)) {
      page.lastModifier["avatar64"] = cache.get(cache_key);
    } else {
      //Add to the request the specific URL for the avatar
      pagesRequest.url = config.globalAuth[config.credentials].server + page.lastModifier.avatarUrl;
      pagesRequest.encoding = "binary";

      //Send the request & analyse the response
      dependencies.request(pagesRequest, function(error, response, avatarData) {
        //If any error
        if (error || !response || (response.statusCode != 200)) {
          //Leave the base64 data empty
        } else {
          //Check if data received is empty
          if (!avatarData) {
            //Leave the base64 data empty
          } else {
            //Get the binary data & convert into base64
            var buf = new Buffer(avatarData, 'binary');
            var avatar64 = 'data:' + response.headers['content-type'] + ';base64,' + buf.toString('base64');
            //Put the base64 data in cache
            cache.put(cache_key, avatar64, avatars_cache_expiration);
            //Put the base64 data in page
            page.lastModifier["avatar64"] = avatar64;
          }
        }
      });
    }
    //Put the updated page into an array
    result.push(page);
    //Return the array containing the updated page
    return callback(null, result);
  };

  // -----------------------
  // Build the playlist based on new items to be displayed (also if @page.id not in cache)
  // -----------------------
  function build_playlist(page, callback) {
    var result = null;
    var alert = false;
    var cache_key = 'confluenceactivity-page-' + page.id + ':id';

    //Check if already in cache
    if (cache.get(cache_key)) {
      //Check if @lastModifiedDate changed
      if (cache.get(cache_key) == page.lastModifiedDate.date) {
        //Do not alert for this page
        alert = false;
      } else {
        //Alert about this page (already in cache but this is a new update about it)
        alert = true;
      }
    } else {
      //Alert about this new page
      alert = true;
    }

    //Remember again this page for the @pages_cache_expiration duration
    cache.put(cache_key, page.lastModifiedDate.date, pages_cache_expiration);

    //If alert should be raised for this page, playlist is defined & playlist includes a music for this space
    if (alert && config.playlist && config.playlist[page.space.key] !== undefined) {
      result = {};
      result.music = config.playlist[page.space.key];
    }

    //Return the array containing the music (if any)
    return callback(null, result);
  };

  // -----------------------
  // MAIN
  // -----------------------
  function job_process() {
    //For each space, call retreive_data function
    async.map(config.spaceKeys, retreive_data, function(err, results) {
      //If any error
      if (err) {
        logger.error(err);
        job_callback(err);
      } else {
        var finalResults = [];
        var now = new Date();
        //Flatten the nested array(s) in order to have all pages at the same level
        var mergedResults = _.flatten(results, true);

        //Sort the array by lastModifiedDate DESC
        mergedResults = _.sortBy(mergedResults, function(page) {
          pageDate = new Date(page.lastModifiedDate.date);
          return now.getTime() - pageDate.getTime();
        });

        //Take the @displaySize first items out & put them into @finalResults
        _.each(mergedResults, function(element, index, list) {
          if (index < displaySize) {
            finalResults.push(element);
          }
        });

        //For page in the final results, call retreive_avatar function to get avatar of the modifier
        async.map(finalResults, retreive_avatar, function(err, results) {
          //If any error
          if (err) {
            logger.error(err);
            job_callback(err);
          } else {
            //Flatten the nested array(s) in order to have all pages at the same level
            results = _.flatten(results, true);

            //For page in the final results, call build_playlist function to get build the playlist to play
            async.map(results, build_playlist, function(err, playlist) {
              if (err) {
                logger.error(err);
                job_callback(err);
              } else {
                if(playlist.lenght > 0) {
                  //Compact the playlist (remove nulls)
                  playlist = _.compact(playlist);
                  //Extract only the "music" property
                  playlist = _.pluck(playlist, 'music');
                  //Get unique values
                  playlist = _.uniq(playlist);
                }
                
                //Populate the job result with required data
                job_callback(null, {
                  server : config.globalAuth[config.credentials].server,
                  pages : results,
                  playlist : playlist
                });
              }
            });

          }
        });
      }
    });
  }

};
