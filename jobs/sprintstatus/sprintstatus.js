/*  ----- CONFIGURATION EXAMPLE -----
 *    "job-configuration": {
 *      "timeout": 30000,
 *      "retryOnErrorTimes": 3,
 *      "interval": 120000,
 *      "credential": "jac",
 *      "rapidBoardId": 500
 *    }
 *
 *  --- Auth configuration ---
 *    "jac": {
 *      "server": "https://jira.atlassian.com",
 *      "username": "username",
 *      "password": "password",
 *      "mode": "-header" OR "-user" OR "-lasso"
 *      "lassoServer": "https://id.atlassian.com" //if "lasso" mode
 *    }
 */
var querystring = require('querystring'), _ = require("underscore"), cache = require('memory-cache'), dateTool = require('./lib/date.js');

var credentials_cache_expiration = 60 * 60 * 1000;

module.exports = function(config, dependencies, job_callback) {

  //Check if "rapidBoardId" & "credential" are defined
  if (!config.projectName || !config.rapidBoardId || !config.credentials) {
    return job_callback('Missing parameters in sprintstatus job');
  }

  //Check if every credential detail is defined
  if (!config.globalAuth || !config.globalAuth[config.credentials] || !config.globalAuth[config.credentials].username || !config.globalAuth[config.credentials].password || !config.globalAuth[config.credentials].server) {
    return job_callback('No credentials found in sprintstatus job. Please check config.globalAuth');
  }

  //In the specifc case of "Lasso" auth mode, check if the "Lasso" server is defined
  if (config.globalAuth[config.credentials].mode == "-lasso" && !config.globalAuth[config.credentials].lassoServer) {
    return job_callback('No Lasso server found in sprintstatus job. Please check config.globalAuth');
  }

  //Get the logger instance
  var logger = dependencies.logger;

  //Define the request made to Greenhopper to get the list of sprints defined in the RapidBoard
  var sprintsRequest = {
    timeout : config.timeout || 15000,
    url : config.globalAuth[config.credentials].server + '/rest/greenhopper/latest/sprints/' + config.rapidBoardId
  };

  //Depending on the selected auth mode, add the requried credentials into the data request @sprintsRequest
  switch(config.globalAuth[config.credentials].mode) {
    case "-lasso":
      var cookieString = "";
      var cache_key = 'sprintstatus-lasso-' + config.globalAuth[config.credentials].lassoServer + '-' + config.globalAuth[config.credentials].username + ':cookie';

      //If cookie in cache
      if (!cache.get(cache_key)) {
        //Create the cookie request to send to Lasso server
        var credentialRequest = {
          timeout : config.timeout || 15000,
          url : config.globalAuth[config.credentials].lassoServer + '/id/rest/login',
          method : "POST",
          json : {
            username : config.globalAuth[config.credentials].username,
            password : config.globalAuth[config.credentials].password
          }
        };

        //Send the request & analyse the response
        dependencies.request.post(credentialRequest, function(error, response, credentialsJSON) {
          //If any error
          if (error || !response || (response.statusCode != 200)) {
            var err_msg = (error || ( response ? ("bad statusCode: " + response.statusCode) : "bad response")) + " from " + credentialRequest.url;
            job_callback(err_msg);
          } else {
            //If cookie is not missing
            if (response.headers["set-cookie"] !== undefined) {
              //Get the cookie
              cookieString = response.headers["set-cookie"];
              //Add cookie into cache
              cache.put(cache_key, cookieString, credentials_cache_expiration);
            } else {
              job_callback("Cannot get the cookie from Lasso server in sprintstatus job.");
            }
          }
          //Put the cookie into the data request
          sprintsRequest.cookie = cookieString;
          //Start the job process with this initialized request
          job_process(sprintsRequest);
        });
      } else {
        //Put the cookie from cache into the data request
        sprintsRequest.cookie = cache.get(cache_key);
        //Start the job process with this initialized request
        job_process(sprintsRequest);
      }
      break;
    case "-user":
      //Put the user credentials into the data request
      sprintsRequest.user = {};
      sprintsRequest.user[config.globalAuth[config.credentials].username] = config.globalAuth[config.credentials].password;
      //Start the job process with this initialized request
      job_process(sprintsRequest);
      break;
    case "-header":
    default:
      //Put the auth header into the data request
      sprintsRequest.headers = {};
      sprintsRequest.headers["Authorization"] = "Basic " + new Buffer(config.globalAuth[config.credentials].username + ":" + config.globalAuth[config.credentials].password).toString("base64");
      //Start the job process with this initialized request
      job_process(sprintsRequest);
      break;
  }

  // -----------------------
  // MAIN
  // -----------------------
  function job_process(sprintsRequest) {
    //Send the request & analyse the response
    dependencies.request(sprintsRequest, function(error, response, sprintsJSON) {
      //If any error
      if (error || !response || (response.statusCode != 200)) {
        var err_msg = (error || ( response ? ("bad statusCode: " + response.statusCode) : "bad response")) + " from " + sprintsRequest.url;
        job_callback(err_msg);
      } else {

        //Populate "sprintsData" with response JSON data
        var sprintsData;
        try {
          sprintsData = JSON.parse(sprintsJSON);
        } catch (err) {
          return job_callback('Error parsing JSON response from server');
        }

        //Check if at least 1 sprint is returned
        if (!sprintsData.sprints || sprintsData.sprints.length == 0) {
          job_callback(null, {
              empty : true,
              projectName : config.projectName,
              sprintName : null,
              daysLeft : null,
              completed : null,
              inprogress : null,
              incompleted : null
            });
        } else {

          //Get the 1st non-closed print, assuming it is the current sprint
          var sprint = _.findWhere(sprintsData.sprints, {
            closed : false
          });

          //Check if there is a non-closed sprint, ie a sprint in progress
          
          if (!sprint) {
            job_callback(null, {
              empty : true,
              projectName : config.projectName,
              sprintName : null,
              daysLeft : null,
              completed : null,
              inprogress : null,
              incompleted : null
            });
          } else {

            //Re-use the previous request, including the credentials
            var sprintDetailsRequest = sprintsRequest;
            //Re-define the request url to get the details about the current sprint
            sprintDetailsRequest.url = config.globalAuth[config.credentials].server + '/rest/greenhopper/latest/rapid/charts/sprintreport?rapidViewId=' + config.rapidBoardId + '&sprintId=' + sprint.id;
            //Send the request & analyse the response
            dependencies.request(sprintDetailsRequest, function(error, response, sprintDetailsJSON) {
              //If any error
              if (error || !response || (response.statusCode != 200)) {
                var err_msg = (error || ( response ? ("bad statusCode: " + response.statusCode) : "bad response")) + " from " + sprintDetailsRequest.url;
                job_callback(err_msg);
              } else {

                //Populate "sprintDetailsData" with response JSON data
                var sprintDetailsData;
                try {
                  sprintDetailsData = JSON.parse(sprintDetailsJSON);
                } catch (err) {
                  return job_callback('Error parsing JSON response from server');
                }

                //Get the number of days left before the end of the sprint
                var daysLeft = 0;
                if (!dateTool.isDate(sprintDetailsData.sprint.endDate, "d/MMM/y h:m a")) {
                  daysLeft = "?";
                } else {
                  var now = new Date(), oneDay = 24 * 60 * 60 * 1000;
                  daysLeft = Math.round((dateTool.getDateFromFormat(sprintDetailsData.sprint.endDate, "d/MMM/y h:m a") - now.getTime()) / (oneDay));
                }

                //Populate the job result with required data
                job_callback(null, {
                  empty : false,
                  projectName : config.projectName,
                  sprintName : sprintDetailsData.sprint.name,
                  daysLeft : daysLeft,
                  completed : sprintDetailsData.contents.completedIssuesEstimateSum.value,
                  inprogress : sprintDetailsData.contents.puntedIssuesEstimateSum.value,
                  incompleted : sprintDetailsData.contents.incompletedIssuesEstimateSum.value
                });
              }
            });
          }
        }
      }
    });
  }

};

