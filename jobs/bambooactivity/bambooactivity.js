/*  ----- CONFIGURATION EXAMPLE -----
 *    "job-configuration": {
 *      "timeout": 30000,
 *      "retryOnErrorTimes": 3,
 *      "interval": 120000,
 *      "credentials" : "credential2",
 *      "showBuilds": ["PROJECT1", "PROJECT2", "PROJECT3"],
 *      "showFullName": false",
 *      "playlist": {   //Sounds to be in "/assets/sounds/"
 *        "PROJECT1": {
 *          "success": "../sounds/sound1.wav",
 *          "failure": "../sounds/sound2.mp3"
 *        },
 *        "PROJECT2": {
 *          "success": "../sounds/sound3.wav",
 *          "failure": "../sounds/sound4.mp3"
 *        }
 *      }
 *    }
 *
 *  --- Auth configuration ---
 *    "credential2": {
 *      "server": "https://cbac.atlassian.com",
 *      "username": "username",
 *      "password": "password",
 *      "mode": "-header" OR "-user" OR "-lasso"
 *      "lassoServer": "https://id.atlassian.com" //if "lasso" mode
 *    }
 */

var cheerio = require('cheerio'), async = require('async'), project_to_plans = require('./lib/project_to_plans'), get_responsible = require('./lib/get_responsible'), get_build_info = require('./lib/get_build_info'), get_plan_info = require('./lib/get_plan_info'), _ = require("underscore");

var plans_cache_expiration = 24 * 60 * 60 * 1000;
var credentials_cache_expiration = 60 * 60 * 1000;

module.exports = function(config, dependencies, job_callback) {

  if (!config.showBuilds || !config.credentials) {
    return job_callback("Missing parameters in bambooactivity job");
  }

  if (!config.globalAuth || !config.globalAuth[config.credentials] || !config.globalAuth[config.credentials].username || !config.globalAuth[config.credentials].password || !config.globalAuth[config.credentials].server) {
    return job_callback('No bamboo credentials found in bambooactivity job. Please check global authentication file');
  }

  //In the specifc case of "Lasso" auth mode, check if the "Lasso" server is defined
  if (config.globalAuth[config.credentials].mode == "-lasso" && !config.globalAuth[config.credentials].lassoServer) {
    return job_callback('No Lasso server found in bambooactivity job. Please check config.globalAuth');
  }

  var logger = dependencies.logger;
  var self = this;
  var showFullName = config.showFullName || false;
  var authOptions = {};

  //Depending on the selected auth mode, add the requried credentials into the auth options @authOptions
  switch(config.globalAuth[config.credentials].mode) {
    case "-lasso":
      var cookieString = "";
      var cache_key = 'bambooactivity-lasso-' + config.globalAuth[config.credentials].lassoServer + '-' + config.globalAuth[config.credentials].username + ':cookie';

      //If cookie in cache
      if (!cache.get(cache_key)) {
        //Create the cookie request to send to Lasso server
        var credentialRequest = {
          timeout : config.timeout || 15000,
          url : config.globalAuth[config.credentials].lassoServer + '/id/rest/login',
          method : "POST",
          json : {
            username : config.globalAuth[config.credentials].username,
            password : config.globalAuth[config.credentials].password
          }
        };

        //Send the request & analyse the response
        dependencies.request.post(credentialRequest, function(error, response, credentialsJSON) {
          //If any error
          if (error || !response || (response.statusCode != 200)) {
            var err_msg = (error || ( response ? ("bad statusCode: " + response.statusCode) : "bad response")) + " from " + credentialRequest.url;
            job_callback(err_msg);
          } else {
            //If cookie is not missing
            if (response.headers !== undefined && response.headers["set-cookie"] !== undefined) {
              //Get the cookie
              cookieString = response.headers["set-cookie"];
              //Add the cookie into cache
              cache.put(cache_key, cookieString, credentials_cache_expiration);
            } else {
              job_callback("Cannot get the cookie from Lasso server in sprintstatus job.");
            }
          }
          //Put the cookie into the auth options
          authOptions.cookie = cookieString;
          //Start the job process
          job_process();
        });
      } else {
        //Put the cookie into the auth options
        authOptions.cookie = cache.get(cache_key);
        //Start the job process
        job_process();
      }
      break;
    case "-user":
      //Put the user credentials into the auth options
      authOptions.user = {};
      authOptions.user[config.globalAuth[config.credentials].username] = config.globalAuth[config.credentials].password;
      //Start the job process
      job_process();
      break;
    case "-header":
    default:
      //Put the auth header into the auth options
      authOptions.headers = {};
      authOptions.headers["Authorization"] = "Basic " + new Buffer(config.globalAuth[config.credentials].username + ":" + config.globalAuth[config.credentials].password).toString("base64");
      //Start the job process
      job_process();
      break;
  }

  // get plan info with extra info if failed build
  function getData(plan, callback) {
    // get plan information from
    get_plan_info(plan, authOptions, config, dependencies.request, function(err, build) {
      var result = {
        link : config.globalAuth[config.credentials].server + "/browse/" + plan,
        planKey : plan,
        planName : plan,
        responsible : [],
        failBuildKey : "",
        isRefreshing : false,
        success : "",
        down : false
      };

      if (err || !build) {
        result.down = true;
        logger.error( err ? ("Error accessing build info for plan " + plan + ": " + err) : "No build info available for plan " + plan);
        // we don´t pass the error to the caller. we just mark it as down.
        return callback(null, result);
      }

      var keysArray = build.plan.key.split("-");
      keysArray.pop();
      result.projectKey = keysArray.join("-");

      if (showFullName) {
        result.planName = build.plan.name;
      } else {
        result.planName = build.planName;
      }

      if (build.state == "Successful") {
        result.success = "successful";
        callback(null, result);
      } else {
        get_build_info(plan, authOptions, config, dependencies.request, function(err, failed_build) {
          if (err || !failed_build) {
            result.down = true;
            logger.error( err ? err : "Error getting build infor for plan " + plan);
            return callback(null, result);
          }

          result.failBuildKey = build.key;
          result.isRefreshing = (failed_build.lifeCycleState == "NotBuilt");

          get_responsible(build.key, authOptions, dependencies.request, config, function(err, users) {
            if (err || !failed_build) {
              result.down = true;
              return callback(null, result);
            }

            result.success = "failed";
            result.responsible = users;
            callback(err, result);
          });
        });
      }
    });
  };

  // -----------------------
  // fetch plans
  // -----------------------
  function check_plans(builds, callback) {
    if (!builds || !builds.length) {
      callback(null, []);
    } else {
      var fetcher = function(build, callback) {
        project_to_plans(build, authOptions, config, dependencies.request, function(err, plans) {
          if (err) {
            logger.error("error accesing build \"" + build + "\": " + err);
            return callback(null, []);
            //we don't want the error to level up.
          }
          callback(null, plans);
        });
      };

      async.map(builds, fetcher, function(err, results) {
        callback(err, _.flatten(results));
      });
    }
  }

  function execute_projects(builds, callback) {
    if (!builds || !builds.length) {
      return callback(null, []);
    }

    check_plans(builds, function(err, plans) {
      if (err || !plans || !plans.length) {
        return callback(err, []);
      }

      async.map(plans, getData, function(err, results) {
        callback(err, results);
      });
    });
  }

  // -----------------------
  // Build the playlist based on new status of builds
  // -----------------------
  function build_playlist(plan, callback) {
    var result = null;
    var alert = false;
    var cache_key = 'buildoverview-plan-' + plan.planKey + ':status';

    //Check if already in cache
    if (cache.get(cache_key)) {
      //Check if @lastModifiedDate changed
      if (cache.get(cache_key) == plan.success) {
        //Do not alert for this page
        alert = false;
      } else {
        //Alert about this page (already in cache but this is a new update about it)
        alert = true;
      }
    } else {
      //Alert about this new page
      alert = true;
    }

    //Remember again this page for the @pages_cache_expiration duration
    cache.put(cache_key, plan.success, plans_cache_expiration);

    //If alert should be raised for this page, playlist is defined & playlist includes a music for this space
    if (alert && config.playlist !== undefined && config.playlist[plan.projectKey] !== undefined) {
      result = {};
      if (plan.success == "successful") {
        result.music = config.playlist[plan.projectKey].success;
      } else {
        result.music = config.playlist[plan.projectKey].failure;
      }
    }
    //Return the array containing the music (if any)
    return callback(null, result);
  };

  // ------------------------------------------
  // MAIN
  // ------------------------------------------

  //sort function for consistent build listing
  function failure_sort(a, b) {
    function score(build) {
      return build.down === true ? 20 : (build.success === "failed" ? 10 : 0);
    }

    return score(a) > score(b);
  };

  function job_process() {
    //var projects = [_.compact(config.failBuilds), _.compact(config.showBuilds)];
    var projects = [_.compact(config.showBuilds)];
    async.map(projects, execute_projects, function(err, results) {
      if (err) {
        logger.error(err);
        job_callback(err);
      } else {
        results = _.flatten(results, true);

        async.map(results, build_playlist, function(err, playlist) {
          if (err) {
            logger.error(err);
            job_callback(err);
          } else {
            if(playlist.lenght > 0) {
              //Compact the playlist (remove nulls)
              playlist = _.compact(playlist);
              //Extract only the "music" property
              playlist = _.pluck(playlist, 'music');
              //Get unique values
              playlist = _.uniq(playlist);
            }

            //Populate the job result with required data
            job_callback(null, {
              showBuilds : results.sort(failure_sort),
              failBuilds : [],
              //showBuilds : results[1].sort(failure_sort),
              //failBuilds : results[0].sort(failure_sort),
              playlist : playlist,
              title : config.widgetTitle,
              fullName : showFullName
            });
          }
        });
      }
    });
  }

};
