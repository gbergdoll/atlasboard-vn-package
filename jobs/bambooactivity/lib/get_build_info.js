var cheerio = require('cheerio'),
    cache = require('memory-cache');

// -------------------------------------------
//  Get build info
// -------------------------------------------
var cache_expiration = 60 * 1000; //ms

module.exports = function(build, authOptions, config, request, callback) {

    var cache_key = 'buildoverview-build-' + build + ':info';
    if (cache.get(cache_key)){
        return callback (null, cache.get(cache_key));
    }

    authOptions.timeout = 15000;
    authOptions.url = config.globalAuth[config.credentials].server + "/rest/api/latest/result/" + build + "-latest.json?includeAllStates";

    request(authOptions, function(err, response, json_body) {
        if (err || !response || response.statusCode != 200) {
            var err_msg = err || "bad response from " + authOptions.url + (response ? " - status code: " + response.statusCode : "");
            callback(err || err_msg);
        } else {
            var build;
            try{
                build = JSON.parse(json_body);
            }
            catch (e) {
                return callback(e, null);
            }
            cache.put(cache_key, build, cache_expiration); //add to cache
            callback(null, build);
        }
    });

};
