widget = {
  //runs when we receive data from the job
  onData : function(el, data) {

    //The parameters our job passed through are in the data object
    //el is our widget element, so our actions should all be relative to that
    $('.content', el).empty();
    
    if (data.empty) {
      $('h2', el).html(data.projectName);
      $('.content', el).append(
        "<div class='no-sprint-message'>" +
          "NO SPRINT IN PROGRESS ?" +
        "</div>");
    } else {
      $('h2', el).html(data.projectName + " - " + data.sprintName);
      
      var table = $("<table/>");
      
      var titleRow = $("<tr/>").addClass("titles");
      titleRow.append($("<td/>").addClass("incompleted-title").html("Not started"));
      titleRow.append($("<td/>").addClass("inprogress-title").html("In progress"));
      titleRow.append($("<td/>").addClass("completed-title").html("Done"));
      titleRow.append($("<td/>").addClass("daysleft-title").html("Days left"));
      table.append(titleRow);
      
      var valuesRow = $("<tr/>").addClass("values");
      valuesRow.append($("<td/>").addClass("incompleted").html(data.incompleted));
      valuesRow.append($("<td/>").addClass("inprogress").html(data.inprogress));
      valuesRow.append($("<td/>").addClass("completed").html(data.completed));
      valuesRow.append($("<td/>").addClass("daysleft").html(data.daysLeft));
      table.append(valuesRow);
      
      $('.content', el).append(table);

      if (data.daysLeft == "?" || data.daysLeft > 0) {
        $('td.daysleft', el).addClass('positive');
      } else {
        $('td.daysleft', el).addClass('negative');
      }
    }
    $('.content', el).removeClass('hidden').addClass('show');
    $('.spinner', el).remove();
  }
};
