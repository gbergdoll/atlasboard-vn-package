widget = {

  onData: function (el, data) {
    $('.activity', el).empty();

    $('h2', el).html("CONFLUENCE ACTIVITY");

    if (data.pages.length > 0) {

      data.pages.forEach(function(page) {
        var pageUrl = _.findWhere(page.link,{"rel": "alternate"}).href;
        
        var avatarSrc = "";
        if (!page.lastModifier.avatar64 || page.lastModifier.avatar64 == "") {
          avatarSrc = data.server + "/wiki/s/en_GB-1988229788/4324/348968a033a373877bb56a79afc508f337ebd5b1.143/_/images/icons/profilepics/default.png";
        } else {
          avatarSrc = page.lastModifier.avatar64;
        } 
        
        var summary = "";
        if(page.createdDate.date == page.lastModifiedDate.date) {
          summary = "Created by " + page.lastModifier.displayName + " • " + page.createdDate.friendly;
        } else {
          summary = "Updated by " + page.lastModifier.displayName + " • " + page.lastModifiedDate.friendly;
        }
        
        var listItem = $("<li/>").append($("<img alt = '" + page.lastModifier.displayName +
            "' title='" + page.lastModifier.displayName +
            "' class='avatar' src='" + avatarSrc + "'/>"));
        var $pageData = $("<div class=\"page-data\"/>");
        
        listItem.append($pageData);
        $pageData.append($("<strong/>").addClass("page-space").append(page.space.key));
        $pageData.append($("<strong/>").addClass("page-title").append("<a href='" + pageUrl + "'>" + page.title + "</a>"));
        listItem.append($("<div/>").addClass("page-summary").append(summary));
        
        $('.activity', el).append(listItem);
      });

    } else {

      $('.activity', el).append(
        "<div class='no-page-message'>" +
          "NO ACTIVITY FOUND" +
          "<div class='smiley-face'>" +
          "☺" +
          "</div>" +
        "</div>");
    }
    
    if (data.playlist.length > 0) {
      var i = 0;
      data.playlist.forEach(function(music) {
        if (music != null) {
          var musicItem = $("<embed id='music-track-" + i + "' autostart='true' src='" + music + "' />").addClass("embed-invisible");
          $('.activity', el).append(musicItem);
          i++;
        }
      });
    }

    $('.content', el).show();
  }

};